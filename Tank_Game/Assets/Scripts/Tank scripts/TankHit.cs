﻿using UnityEngine;

public class TankHit : MonoBehaviour
{
    public GameObject shootingGameObject; // gets the object the fired the bullet
    private GameObject hitGameObject; // gets the object that was hit

    void OnCollisionEnter(Collision other)
    {
        hitGameObject = other.gameObject; // sets the hit object to the object that was collided
        if (other.gameObject.GetComponent<TankData>())
        {
            TankData shooterData = shootingGameObject.GetComponent<TankData>(); // gets the tank data of the shooter
            TankData hitData = hitGameObject.GetComponent<TankData>(); // gets the tank data of the object that was hit

            if (gameObject.tag == "Player")
            {
                if (other.gameObject.tag == "Enemy")
                {
                    hitData.TakeDamage(shooterData.shellDamage); // makes the enemy take damage

                    if (hitData.health <= 0)
                    {
                        shooterData.AddScore(hitData.pointValue); // adds score to the player
                    }

                    Destroy(gameObject); // destroys the bullet
                }
            }

            if (gameObject.tag == "Enemy")
            {
                if (other.gameObject.tag == "Player")
                {
                    hitData.TakeDamage(shooterData.shellDamage); // makes the player take damage

                    if (hitData.health <= 0)
                    {
                        shooterData.AddScore(hitData.pointValue); // adds score to the enemy

                    }

                    Destroy(gameObject); // destroys the bullet
                }
            }
        }
    }
}
