﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PowerUp
{
    public float speedModifier;
    public float healthModifier;
    public float MaxHealthModifier;
    public float FireRateModifier;

    public float Duration;
    public bool IsPermanent;

    public void OnActivate(TankData data)
    {
        data.forwardSpeed += speedModifier;
        data.health += healthModifier;
        data.maxHealth += MaxHealthModifier;
        data.firerate += FireRateModifier;
    }

    public void OnDeactivate(TankData data)
    {
        data.forwardSpeed -= speedModifier;
        data.health -= healthModifier;
        data.maxHealth -= MaxHealthModifier;
        data.firerate -= FireRateModifier;
    }
}
