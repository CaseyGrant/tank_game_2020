﻿using UnityEngine;

public class TankController : MonoBehaviour
{
    private TankData Data; // gets the tanks data script
    private TankMotor Motor; // gets the tanks motor script
    private TankShoot Shoot; // gets the tanks shoot script

    void Start()
    {
        Data = GetComponent<TankData>(); // allows you to access the data script
        Motor = GetComponent<TankMotor>(); // allows you to access the motor script
        Shoot = GetComponent<TankShoot>(); // allows you to access the shoot  script
    }

    void Update()
    {
        switch (Data.input)
        {
            case TankData.InputScheme.Arrow_Keys: // When using the arrow keys

                if (Input.GetKey(KeyCode.UpArrow))
                {
                    Motor.Move(Data.forwardSpeed); // move forward
                }

                if (Input.GetKey(KeyCode.DownArrow))
                {
                    Motor.Move(-Data.backwardSpeed); // move backward
                }

                if (Input.GetKey(KeyCode.RightArrow))
                {
                    Motor.Rotate(Data.roatationSpeed); // rotate right
                }

                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    Motor.Rotate(-Data.roatationSpeed); // rotate left
                }
                else
                {
                    Motor.Move(0); // updates physics
                }

                if (Input.GetKey(KeyCode.RightControl))
                {
                    Shoot.Shoot(); // shoots a bullet
                }

                break; // ends the case

            case TankData.InputScheme.WASD: // when using WASD

                if (Input.GetKey(KeyCode.W))
                {
                    Motor.Move(Data.forwardSpeed); // move forward
                }

                if (Input.GetKey(KeyCode.S))
                {
                    Motor.Move(-Data.backwardSpeed); // move backward
                }

                if (Input.GetKey(KeyCode.D))
                {
                    Motor.Rotate(Data.roatationSpeed); // rotate right
                }

                if (Input.GetKey(KeyCode.A))
                {
                    Motor.Rotate(-Data.roatationSpeed); // rotate left
                }
                else
                {
                    Motor.Move(0); // updates physics
                }

                if (Input.GetKey(KeyCode.Space))
                {
                    Shoot.Shoot(); // shoots a bullet
                }

                break; // ends the case
        }
    }
}
