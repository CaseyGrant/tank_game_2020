﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : MonoBehaviour
{
    public PowerUp CheatPowerUp;
    private PowerUpController CheatPowerUpController;

    // Start is called before the first frame update
    void Start()
    {
        if (CheatPowerUpController == null)
        {
            CheatPowerUpController = GetComponent<PowerUpController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            CheatPowerUpController.Add(CheatPowerUp);
        }
    }
}
