﻿using UnityEngine;

public class TankShoot : MonoBehaviour
{
    private TankData Data; // gets the tanks data script

    void Start()
    {
        Data = GetComponent<TankData>(); // allows you to access the data script
    }

    public void Shoot()
    {
        if (Time.time > Data.nextFire) // continues if next fire is less than time
        {
            GameObject bullet; // creates storage for an object

            Data.nextFire = Time.time + Data.firerate; // resets next fire

            if (gameObject.tag == "Player")
            {
                bullet = Instantiate(Data.shell, Data.shotSpawn.position, Quaternion.identity); // creates a player bullet
            }
            else
            {
                bullet = Instantiate(Data.enemyShell, Data.shotSpawn.position, Quaternion.identity); // creates an enemy bullet
            }

            TankHit shell = bullet.GetComponent<TankHit>(); // gets the script on the bullet
            shell.shootingGameObject = Data.gameObject; // sets the shooting object
            
            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * Data.shellForce); // moves the bullet
            Destroy(bullet, Data.decayRate); // kills the bullet
        }
    }
}
