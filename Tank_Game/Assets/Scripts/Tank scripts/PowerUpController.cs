﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    public List<PowerUp> PowerUps;
    public List<PowerUp> PermanentPowerUps;
    public List<PowerUp> ExpiredPowerUps;
    private TankData data;

    // Start is called before the first frame update
    void Start()
    {
        PowerUps = new List<PowerUp>();

        data = GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
        List<PowerUp> ExpiredPowerUps = new List<PowerUp>();

        foreach (PowerUp power in PowerUps)
        {
            power.Duration -= Time.deltaTime;

            if (power.Duration <= 0)
            {
                ExpiredPowerUps.Add(power);
            }
        }

        foreach (PowerUp power in ExpiredPowerUps )
        {
            power.OnDeactivate(data);
            PowerUps.Remove(power);
        }
        ExpiredPowerUps.Clear();
    }

    public void Add(PowerUp powerUp)
    {
        powerUp.OnActivate(data);
        if (!powerUp.IsPermanent)
        {
            PowerUps.Add(powerUp);
        }
        else
        {
            PermanentPowerUps.Add(powerUp);
        }
    }
}
