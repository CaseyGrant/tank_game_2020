﻿using UnityEngine;

public class TankMotor : MonoBehaviour
{
    private Transform tf; // gets the transform
    private CharacterController characterController; // gets the character controller

    void Start()
    {
        tf = GetComponent<Transform>(); // allows you to access the transform
        characterController = GetComponent<CharacterController>(); // allows you to access the character controller
    }

    public void Move(float speed)
    {
        Vector3 speedVector = tf.forward * speed; // sets how fast the tank moves

        characterController.SimpleMove(speedVector); // moves the tank
    }

    public void Rotate(float speed)
    {
        Vector3 rotateVector = Vector3.up * speed * Time.deltaTime; // sets how fast the tank rotates

        tf.Rotate(rotateVector, Space.Self); // rotates the tank
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget = target - tf.position;

        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

        if (targetRotation == tf.rotation)
        {
            return false;
        }

        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);

        return true;
    }
}