﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public PowerUp powerUp;
    public AudioClip FeedBackAudioClip;
    private Transform tf;

    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        PowerUpController powerUpController = other.GetComponent<PowerUpController>();

        if (powerUpController != null)
        {
            powerUpController.Add(powerUp);
            
            if (FeedBackAudioClip != null)
            {
                AudioSource.PlayClipAtPoint(FeedBackAudioClip, tf.position, 1.0f);
            }

            Destroy(this.gameObject);
        }
    }
}