﻿using System;
using System.Xml;
using UnityEditor.VersionControl;
using UnityEditorInternal;
using UnityEngine;

public class AIController : MonoBehaviour
{
    private Transform tf;
    private TankData Data; // gets the tanks data script
    private TankMotor Motor; // gets the tanks motor script
    private TankShoot Shoot; // gets the tanks shoot script

    public enum Personalities { Inky, Pinky, Blinky, Clyde }
    public Personalities personalities;

    public enum AIStates { Chase, ChaseAndFire, CheckForFlee, Flee, Rest, Patrol }
    public AIStates aiStates;

    public enum LoopTypes { Stop, Loop, PingPong }
    public LoopTypes loopTypes;

    public enum Avoid { Rotate, Move, None }
    public Avoid avoidanceStage;

    public Transform target;
    public Transform[] wayPoints;

    public int currentWayPoint = 0;
    public float closeEnough;
    public bool isPatrolForward = true;
    public float fleeDistance;

    public float avoidanceTime = 2.0f;
    private float exitTime;
    private float stateEnterTime;
    private float healthRegenPerSecond = 1;

    public float hearingDistance;
    public float FOVAngle;
    public float inSightsAngle;


    void Start()
    {
        tf = GetComponent<Transform>();
        Data = GetComponent<TankData>(); // allows you to access the data script
        Motor = GetComponent<TankMotor>(); // allows you to access the motor script
        Shoot = GetComponent<TankShoot>(); // allows you to access the shoot  script
    }

    void Update()
    {

        switch (personalities)
        {
            case Personalities.Inky:
                Inky();
                break;
            case Personalities.Pinky:
                Pinky();
                break;
            case Personalities.Blinky:
                Blinky();
                break;
            case Personalities.Clyde:
                Clyde();
                break;
        }
    }
    private void Inky()
    {
        switch (aiStates)
        {
            case AIStates.Chase:
                DoChase();

                if (Data.health < (Data.maxHealth * 0.5))
                {
                    ChangeState(AIStates.CheckForFlee);
                }
                else if (PlayerIsInRange())
                {
                    ChangeState(AIStates.ChaseAndFire);
                }

                break;

            case AIStates.ChaseAndFire:
                DoChase();
                Shoot.Shoot();

                if (Data.health < (Data.maxHealth * 0.5))
                {
                    ChangeState(AIStates.CheckForFlee);
                }
                else if (!PlayerIsInRange())
                {
                    ChangeState(AIStates.Chase);
                }

                break;

            case AIStates.CheckForFlee:
                if (PlayerIsInRange())
                {
                    ChangeState(AIStates.Flee);
                }
                else
                {
                    ChangeState(AIStates.Rest);
                }

                break;

            case AIStates.Flee:
                Flee();
                ChangeState(AIStates.CheckForFlee);
                break;

            case AIStates.Rest:
                Rest();

                if (Time.time >= stateEnterTime + 30f)
                {
                    ChangeState(AIStates.CheckForFlee);
                }

                if (PlayerIsInRange())
                {
                    ChangeState(AIStates.Flee);
                }
                else if (Data.health == Data.maxHealth)
                {
                    ChangeState(AIStates.Chase);
                }

                break;
        }
    }

    private void Clyde()
    {
        switch (aiStates)
        {
            case AIStates.Patrol:
                Patrol();

                if (PlayerIsInRange())
                {
                    ChangeState(AIStates.ChaseAndFire);
                }
                break;

            case AIStates.ChaseAndFire:
                DoChase();
                Shoot.Shoot();
                if (!PlayerIsInRange())
                {
                    ChangeState(AIStates.Patrol);
                }

                break;
        }
    }

    private void Blinky()
    {
        switch (aiStates)
        {
            case AIStates.ChaseAndFire:
                DoChase();
                Shoot.Shoot();

                if (PlayerIsInRange())
                {
                    ChangeState(AIStates.Flee);
                }
                break;

            case AIStates.Flee:
                if (CanMove(Data.forwardSpeed))
                {
                    Flee();
                }
                else
                {
                    DoAvoid();
                }
                
                if (!PlayerIsInRange())
                {
                    ChangeState(AIStates.ChaseAndFire);
                }
                break;
        }
    }

    private void Pinky()
    {
        switch (aiStates)
        {
            case AIStates.Flee:
                Flee();

                if (PlayerIsInRange())
                {
                    ChangeState(AIStates.Chase);
                }
                break;
        }
    }

    private void Patrol()
    {
        if (Motor.RotateTowards(wayPoints[currentWayPoint].position, Data.roatationSpeed))
        {

        }
        else
        {
            Motor.Move(Data.forwardSpeed);
        }

        if (Vector3.SqrMagnitude(wayPoints[currentWayPoint].position - tf.position) <= (closeEnough * closeEnough))
        {
            switch (loopTypes)
            {
                case LoopTypes.Stop:

                    if (currentWayPoint < wayPoints.Length - 1)
                    {
                        currentWayPoint++;
                    }

                    break;

                case LoopTypes.Loop:

                    if (currentWayPoint < wayPoints.Length - 1)
                    {
                        currentWayPoint++;
                    }
                    else
                    {
                        currentWayPoint = 0;
                    }

                    break;

                case LoopTypes.PingPong:

                    if (isPatrolForward)
                    {
                        if (currentWayPoint < wayPoints.Length - 1)
                        {
                            currentWayPoint++;
                        }
                        else
                        {
                            isPatrolForward = false;
                            currentWayPoint--;
                        }
                    }
                    else
                    {
                        if (currentWayPoint > 0)
                        {
                            currentWayPoint--;
                        }
                        else
                        {
                            isPatrolForward = true;
                            currentWayPoint++;
                        }
                    }

                    break;
            }
        }
    }

    private void Rest()
    {
        Data.health += healthRegenPerSecond * Time.deltaTime;
        if (Data.health >= Data.maxHealth)
        {
            ChangeState(AIStates.Chase);
        }
    }

    private void Flee()
    {
        Vector3 vectorToTarget = target.position - tf.position;
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;
        vectorAwayFromTarget.Normalize();
        vectorAwayFromTarget *= fleeDistance;

        Vector3 fleePosition = vectorAwayFromTarget + tf.position;

        Motor.RotateTowards(fleePosition, Data.roatationSpeed);
        Motor.Move(Data.forwardSpeed);
    }

    private bool PlayerIsInRange()
    {
        if (Vector3.SqrMagnitude(target.position - tf.position) <= (hearingDistance * hearingDistance))
        {
            return true;
        }

        return false;
    }

    private void DoChase()
    {
        Motor.RotateTowards(target.position, Data.roatationSpeed);

        if (CanMove(Data.forwardSpeed))
        {
            Motor.Move(Data.forwardSpeed);
        }
        else
        {
            avoidanceStage = Avoid.Rotate;
        }
    }

    private void DoAvoid()
    {
        if (avoidanceStage == Avoid.Rotate)
        {
            Motor.Rotate(Data.roatationSpeed);
            if (CanMove(Data.forwardSpeed))
            {
                avoidanceStage = Avoid.Move;
                exitTime = avoidanceTime;
            }
        }

        if (avoidanceStage == Avoid.Move)
        {
            if (CanMove(Data.forwardSpeed))
            {
                exitTime -= Time.deltaTime;
                Motor.Move(Data.forwardSpeed);

                if (exitTime <= 0)
                {
                    avoidanceStage = Avoid.None;
                }
            }
            else
            {
                avoidanceStage = Avoid.Rotate;
            }
        }
    }

    public bool CanMove(float speed)
    {
        RaycastHit hit;

        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            if (!hit.collider.CompareTag("Enemy"))
            {
                // ... then we can't move
                return false;
            }
        }

        return true;
    }

    public void ChangeState(AIStates newState)
    {
        aiStates = newState;
        stateEnterTime = Time.time;
    }
}