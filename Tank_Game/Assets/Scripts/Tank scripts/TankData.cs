﻿using UnityEngine;

public class TankData : MonoBehaviour
{
    public enum InputScheme // creates an enum
    {
        WASD, // names the enum after what input it will control
        Arrow_Keys // names the enum after what input it will control
    }

    public InputScheme input = InputScheme.WASD; // sets a default input type

    public float forwardSpeed; // sets how fast you go forward
    public float backwardSpeed; // sets how fast you go backward
    public float roatationSpeed; // sets how fast you rotate
    public float shellForce = 1; // sets how fast your bullet goes
    public int shellDamage = 1; // sets how much damage you do
    public float firerate = 1; // sets how fast you can shoot
    public float nextFire; // sets how long until you can shoot again
    public float decayRate; // sets how long the bullet will live
    public float health; // sets the current health
    public float maxHealth = 10; // sets the max health
    public int score = 0; // sets the score
    public int pointValue; // sets how many points you get for a kill

    public GameObject shell; // gets the player bullet
    public GameObject enemyShell; // gets the enemy bullet
    public Transform shotSpawn; // gets where to spawn the bullet

    void Start()
    {
        health = maxHealth; // makes current health max health
    }

    public void TakeDamage(int damage)
    {
        health -= damage; // decreases the health by the shell damage

        if (health <= 0)
        {
            Destroy(gameObject); // destroys the game object
        }
    }

    public void AddScore(int points)
    {
        score += points; // increases the score by the amount of points you get for a kill
    }
}
