﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance; // creates a reference to the game manager instance

    public GameObject levelObject;
    public GameObject instantiatedPlayerTank; // gets the player tank
    public GameObject playerTankPrefab;
    public List<GameObject> enemyTankPrefabs;
    public List<GameObject> instantiatedEnemyTanks; // gets all of the enemies and stores them in an array
    public List<GameObject> enemySpawnPoints;
    public List<GameObject> playerSpawnPoints;

    void Awake()
    {
        if (instance == null) // if instance is empty
        {
            instance = this; // make this the instance
        }
        else
        {
            Debug.LogError("ERROR: There can only be one GameManager."); // lets the developer know that something went wrong
            Destroy(gameObject); // destroys any excess game managers
        }
        DontDestroyOnLoad(gameObject); // makes the game manager persist through loading
    }

    public void Update()
    {
        //if (instantiatedPlayerTank == null)
        //{
        //    SpawnPayer(RandomSpawnPoint(playerSpawnPoints));
        //}
    }

    public void SpawnPayer(GameObject spawnPoint)
    {
        instantiatedPlayerTank = Instantiate(playerTankPrefab, spawnPoint.transform.position, Quaternion.identity);
    }

    public GameObject RandomSpawnPoint(List<GameObject> spawnPoints)
    {
        int spawnToGet = UnityEngine.Random.Range(0, spawnPoints.Count - 1);
        return spawnPoints[spawnToGet];
    }

    public void SpawnEnemy()
    {
        for (int i = 0; i < enemyTankPrefabs.Count; i++)
        {
            GameObject instantiatedTank = Instantiate(enemyTankPrefabs[i], RandomSpawnPoint(enemySpawnPoints).transform.position, Quaternion.identity);
            instantiatedEnemyTanks.Add(instantiatedTank);
        }
    }
}
