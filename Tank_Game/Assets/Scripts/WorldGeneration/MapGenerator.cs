﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum MapType { Seeded, Random, MapOfTheDay }

    public MapType mapType = MapType.Random;
    public int rows;
    public int columns;

    public float rowWidth = 50.0f;
    public float rowHeight = 50.0f;

    public int mapSeed;

    public GameObject[] gridPrefabs;

    private Room[,] grid;

    void Start()
    {
        GameManager.instance.levelObject = this.gameObject;

        switch (mapType)
        {
            case MapType.Random:
                mapSeed = DateToInt(DateTime.Now);
                break;
            case MapType.Seeded:

                break;
            case MapType.MapOfTheDay:
                mapSeed = DateToInt(DateTime.Now.Date);
                break;
            default:
                Debug.Log("[MapGenerator] That map type is not implemented");
                break;
        }
        GenerateGrid();
        GameManager.instance.SpawnPayer(GameManager.instance.RandomSpawnPoint(GameManager.instance.playerSpawnPoints));
        GameManager.instance.SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public void GenerateGrid()
    {
        UnityEngine.Random.seed = mapSeed;
        grid = new Room[columns, rows];

        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                float xPosition = rowWidth * column;
                float zPosition = rowHeight * row;
                Vector3 newPosition = new Vector3( xPosition, 0.0f, zPosition);

                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity);

                tempRoomObj.transform.parent = this.transform;
                tempRoomObj.name = "Room_" + column + ", " + row;

                Room tempRoom = tempRoomObj.GetComponent<Room>();
                grid[column, row] = tempRoom;

                if (row == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (row == rows - 1)
                {
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                ///////////////////////////////////////////
                if (column == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (column == columns - 1)
                {
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
            }
        }
    }

    public int DateToInt(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + 
               dateToUse.Hour + dateToUse.Minute + dateToUse.Second + 
               dateToUse.Millisecond;
    }
}